USE Colegio;

DELIMITER $$
 
CREATE PROCEDURE  InsertarUsuario
(
	in _Nick varchar(50),
	in _Pass varchar(50),
	in _TipoUsuario int
)
BEGIN
insert into Usuario(nick,contrasena,idTipoUsuario) values(_Nick,_Pass,_TipoUsuario);
END$$

DELIMITER ;


DELIMITER $$
 
CREATE PROCEDURE  InsertarAlumno
(
	in _Nombre varchar(50),
	in _Apellido varchar(50),
	in _Imagen varchar(100)
)
BEGIN
insert into Alumno(nombre,apellido,idUsuario,imagen) values(_Nombre,_Apellido,(Select Max(idUsuario) as id from Usuario),_Imagen);
END$$

DELIMITER ;

