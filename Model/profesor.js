var database = require("./database");
var Profesor = {};
Profesor.getAll = function(callback){
    if(database){
        database.query("SELECT * FROM Profesor",
        function(error,resultado){
            if(error){
                throw error;
            }else{
                callback(null,resultado);
            }
        });
    }
}
Profesor.insertar = function(data,callback){
    if(database){
        var values = [];
        var sql = "";
        database.query(sql,values,
        function(error,resultado){
            if(error){
                throw error;

            }else{
                callback(null,{"insertId":resultado.insertId});
            }
        })
    }
}
Profesor.update = function(data,callback){
    if(database){
        var values = [];
        var sql = "";
        database.query(sql,values,
        function(error,resultado){
            if(error){
                throw error;
            } else{
                callback(null,{"insertId":resultado.insertId});
            }
        })
    }
}

Profesor.delete = function(idProfesor,callback){
    if(database){
        var sql = "";
        database.query(sql,idProfesor,
        function(error,resultado){
            if(error){
                throw error;
            }else{
                callback(null, {"Mensaje": "Eliminado"});
            }
        })
    }
}
module.exports = Profesor;