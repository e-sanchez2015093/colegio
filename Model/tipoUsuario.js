var database = require("./database");
var tipoUsuario = {};

tipoUsuario.getTipos = function(callback){
    if(database){
        database.query("SELECT * FROM TipoUsuario",
        function(error,resultado){
            if(error){
                throw error;
            }else{
                callback(null,resultado);
            }
        });
    }
}
module.exports = tipoUsuario;