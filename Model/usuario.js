var database = require("./database");
var Usuario = {};
Usuario.autenticar = function(data,callback){
    if(database){
        var sql = "SELECT * FROM Usuario WHERE nick = ? AND contrasena = ?";
        database.query(sql,[data.nick,data.contrasena],
        function(error,resultado){
            if(error){
                throw error;
            }else{
                callback(null,resultado);
            }
        });
    }
}
Usuario.insert = function(data,callback){
    if(database) {
        var valuess = [data.nick,data.contrasena,data.tipoUsuario];
        var sql = "CALL InsertarUsuario(?, ?, ?)";
        database.query(sql,valuess,
        function(error,resultado){
            if(error) {
                throw error;
            }else{
                callback(null,{"insertId":resultado.insertId});
            }
        });

    }
}
Usuario.getAll= function(callback){
    if(database){
        database.query("SELECT * FROM Usuario",
        function(error,resultado){
            if(error){
                throw error;
            }else{
                callback(null,resultado);
            }

        })
    }
}



module.exports = Usuario;