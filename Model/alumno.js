var database = require("./database");
var alumno = {};

alumno.selectAll = function(callback) {
    if(database) {
        database.query("SELECT * FROM alumno", function(error, resultados){
            if(error) {
                throw error;
            } else {
                callback(null, resultados);
            }

        });
    }
}
alumno.getAlumno = function(idAlumno, callback){
    if(database) {
        var sql = "SELECT * FROM alumno WHERE = ?";
        database.query(sql, idAlumno, function(error, resultado){
            if(error) {
                throw error;
            } else {

                callback(null, resultado);
            }

        });
    }

}

alumno.insert = function(data,callback) {
    if(database) {
        var values = [data.nombre, data.apellido, data.idUsuario, data.imagen];
        var sql  = "INSERT INTO alumno(nombre,apellido, idUsuario, imagen) VALUES(?, ?, ?, ?)";    
        database.query(sql, values, 
        function(error, resultado){
            if(error) {
                throw error;

            } else {
                callback(null, {"insertId": resultado.insertId});
            }
        });   
    }

}
alumno.update = function(data, callback) {
    if(database) {
        var values = [data.nombre, data.apellido, data.idUsuario, data.imagen, data.idAlumno];
        var sql = "UPDATE alumno set nombre = ?, apellido = ? , idUsuario = ?, imagen = ? WHERE idAlumno = ?";
        database.query(sql, values, 
        function(error, result){
            if(error) {
                throw error;
            } else {
                callback(null, {"insertId": result.insertId});
            }
        });
  }

}
alumno.delete = function(idAlumno, callback) {
    if(database){
        var sql = "DELETE FROM alumno WHERE idAlumno = ?";
        database.query(sql, idAlumno, function(err, resultado){
            if(error){
                throw error;
            } else {
                callback(null, {"Mensaje": "Eliminado"})
            }

        })
    }

}
module.exports = alumno;