CREATE DATABASE Colegio;
Use Colegio;
CREATE TABLE TipoUsuario(
	idTipoUsuario INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) not null,
    primary key(idTipoUsuario)
);
CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
    nick varchar(100) not null,
    contrasena varchar(100) not null,
    idTipoUsuario int not null,
    primary key(idUsuario),
	FOREIGN KEY(idTipoUsuario) REFERENCES TipoUsuario(idTipoUsuario)
    
);
CREATE TABLE Token(
	idToken INT NOT NULL AUTO_INCREMENT,
    token varchar(200) not null,
    primary key(idToken)
    
);

CREATE TABLE Alumno(
	idAlumno INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) not null,
    apellido varchar(100) not null,
    idUsuario int not null,
    imagen varchar(100),
    primary key(idAlumno),
    FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario)
);
CREATE TABLE DetalleAlumno(
	idDetalleAlumno INT NOT NULL AUTO_INCREMENT,
    idToken int not null,
    idAlumno int not null,
    primary key(idDetalleAlumno),
    FOREIGN KEY(idToken) REFERENCES Token(idToken),
    FOREIGN KEY(idAlumno) REFERENCES Alumno(idAlumno)
);
CREATE TABLE Profesor(
	idProfesor INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50) not null,
    apellido varchar(50) not null,
    direccion varchar(100) not null,
    telefonoo varchar(20) not null,
    idUsuario int not null,
    primary key(idProfesor),
    foreign key(idUsuario) REFERENCES Usuario(idUsuario)

);

CREATE TABLE Grado(
	idGrado INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idGrado)
);
CREATE TABLE Jornada(
	idJornada INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idJornada)
);
CREATE TABLE Seccion(
	idSeccion INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    idGrado int not null,
    idJornada int not null,
    primary key(idSeccion),
    foreign key(idGrado) REFERENCES Grado(idGrado),
    foreign key(idJornada) REFERENCES Jornada(idJornada)
    
);
CREATE TABLE Materia(
	idMateria INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idMateria)
);
CREATE TABLE Bimestre(
	idBimestre INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idBimestre)
);
CREATE TABLE Actividad(
	idActividad INT NOT NULL AUTO_INCREMENT,
    idMateria INT NOT NULL,
    descripcion varchar(100) not null,
    fechaInicio datetime,
    fechaFin datetime,
    idBimestre int not null,
    valor int not null,
    primary key(idActividad),
    FOREIGN KEY(idBimestre) REFERENCES Bimestre(idBimestre)
    
);
CREATE TABLE Carrera(
	idCarrera INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50) not null,
    primary key(idCarrera)
);
CREATE TABLE SeccionAlumno(
	idSeccionAlumno INT NOT NULL AUTO_INCREMENT,
    idSeccion INT NOT NULL,
    idAlumno INT NOT NULL,
    PRIMARY KEY(idSeccionAlumno)
);
CREATE TABLE MateriaProfesor(
	idMateriaProfesor INT NOT NULL AUTO_INCREMENT,
    idMateria INT NOT NULL,
    idProfesor INT NOT NULL,
    PRIMARY KEY(idMateriaProfesor)
);
CREATE TABLE ActividadAlumno(
	idActividadAlumno INT NOT NULL AUTO_INCREMENT,
    idAlumno INT NOT NULL ,
    idActividad INT NOT NULL,
    archivo VARCHAR(100),
    PRIMARY KEY(idActividadAlumno),
    FOREIGN KEY(idAlumno) REFERENCES Alumno (idAlumno),
    FOREIGN KEY(idActividad) REFERENCES Actividad(idActividad)
);
CREATE TABLE Nota(
	idNota INT NOT NULL AUTO_INCREMENT,
    idAlumno INT NOT NULL,
    idBimestre INT NOT NULL,
    idMateria INT NOT NULL,
    nota INT NOT NULL,
    PRIMARY KEY(idNota),
    FOREIGN KEY(idAlumno) REFERENCES Alumno(idAlumno),
    FOREIGN KEY(idBimestre) REFERENCES Bimestre(idBimestre),
    FOREIGN KEY(idMateria) REFERENCES Materia(idMateria)
);

CREATE TABLE CalificacionAlumno(
	idCalificacionAlumno INT NOT NULL AUTO_INCREMENT,
    idBimestre INT NOT NULL,
    idMateria INT NOT NULL,
    idActividad INT NOT NULL,
    idAlumno INT NOT NULL,
    nota INT NOT NULL,
    primary key(idCalificacionAlumno),
    FOREIGN KEY(idBimestre) REFERENCES Bimestre(idBimestre),
    FOREIGN KEY(idMateria) REFERENCES Materia(idMateria),
    FOREIGN KEY(idActividad) REFERENCES Actividad(idActividad),
    FOREIGN KEY(idAlumno) REFERENCES Alumno(idAlumno)
);

CREATE TABLE Boleta(
	idBoleta INT NOT NULL AUTO_INCREMENT,
	idCalificacionAlumno int ,
    idBimestre int,
    primary key(idBoleta),
    FOREIGN KEY(idCalificacionAlumno) REFERENCES CalificacionAlumno(idCalificacionAlumno)
);


