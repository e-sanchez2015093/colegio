var express = require('express');
var TipoUsuario = require('../model/tipoUsuario');
var router = express.Router();

router.get('/api/tipoUsuario/',function(req, res){
    TipoUsuario.getTipos(function(error,resultado){
        if(typeof resultado !== undefined){
            res.json(resultado);
        }else{
            res.json({"Mensaje":"No hay usuarios"});
        }
    });
});

module.exports = router;