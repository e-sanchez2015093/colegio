var express = require('express');
var router = express.Router();
var Autenticacion = require('../helper/autenticacion');
var auth = new Autenticacion;

/* GET home page. */
router.get('/', function(req, res, next) {
  auth.autorizar(req);
  res.render(auth.getPath() + 'Index');
});
router.get('/cookies', function(req, res, next) {
  res.status(200).send(req.cookies);
});

router.get('/autenticar',function(req,res){
	res.render("default/autenticar");
});
router.get('/cerrar', function(req, res) {
  res.clearCookie('idUsuario');
  res.clearCookie('nick');
  res.redirect('/');
});

module.exports = router;
