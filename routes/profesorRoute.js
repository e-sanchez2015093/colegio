var express = require('express');
var Profesor = require('../model/profesor');
var router = express.Router();

router.get('/api/profesor/',function(req,res){
    Profesor.getAll(function(error,resultado){
        if(typeof resultado !== undefined){
            res.json(resultado);
        }else{
            res.json({"Mensaje":"No hay profesores"});
        }
    });
});
router.post('/api/profesor',function(req,res){
    var data = {
        nombre:req.body.nombre,
        apellido:req.body.apellido,
        direccion:req.body.direccion,
        telefono:req.body.telefono,
        idUsuario:req.body.idUsuario
    }
    Profesor.insertar(data,function(err,resultado){
        if(resultado && resultado.insertId > 0){
            res.redirect('/api/profesor');
        }else{
            res.json({"Mensaje":"No se ingreso el profesor"});
        }
    });
});
router.put('/api/profesor/:idProfesor',function(req,res){
    var idProfesor = req.params.idProfesor;
    var data = {
        idProfesor:req.body.idProfesor,
        nombre:req.body.nombre,
        apellido:req.body.apellido,
        direccion:req.body.direccion,
        telefono:req.body.telefono,
        idUsuario:req.body.idUsuario
    }
    if(idProfesor == data.idProfesor){
        Profesor.update(data,function(err,resultado){
            if(resultado !== undefined){
                res.json(resultado);
            }else{
                res.json({"Mensaje":"No se modifico el profesor"});
            }
        });
    }else{
        res.json({"Mensaje":"No hay coincidencia en los datos"});
    }
});
router.delete('/api/profesor/:idProfesor',function(req,res){
    var idProfesor = req.params.idProfesor;
    Profesor.delete(idProfesor,function(error,resultado){
        if(resultado && resultado.Mensaje === "Eliminado"){
            res.redirect("/api/profesor");
        }else{
            res.json({"Mensaje":"No se puede eliminar"});
        }

    });
});

module.exports = router;