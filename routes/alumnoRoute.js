var express = require('express');
var Alumno = require('../model/alumno');
var router = express.Router();


router.get('/api/alumno', function(req, res) {
    if(auth.getAcceso()) {
        alumno.selectAll(function( error, resultado){
            if(typeof resultado !== undefined) {
                res.json(resultado);
            } else {
                res.json({"Mensaje": "No hay alumnos"});
            }
        });
    }

});
router.get('/api/alumno/:idAlumno', function(req, res){
    var idAlumno = req.params.idAlumno;
    alumno.getAlumno(idAlumno, function(error, resultados){
        if(typeof resultados !== undefined){
            res.json(resultados);
        } else { 
            res.json({ "Mensaje" : "No hay alumno"});
        }

    });

});
router.post('/api/alumno', function(req, res) {
    var data = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        idUsuario: req.body.idUsuario,
        imagen: req.body.imagen
    }
    alumno.insert(data, function(error, resultado){ 
        if((resultado) && (resultado.insertId) > 0) {
            res.redirect("/api/Alumno");

        } else {
            res.json({"Mensaje": "No se ingreso alumno"});
        }

    });
});

router.put('api/alumno/:idAlumno', function(req, res){ 
    var idAlumno = req.params.idAlumno;
      var data = {
       idAlumno: req.body.idAlumno,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        idUsuario: req.body.idUsuario,
        imagen: req.body.imagen
    }
    if(idAlumno == data.idAlumno){
        alumno.update(data, function(err, resultado){
            if(resultado == undefined){
                res.json(resultado);
            } else {
                res.json({"Mensaje": " no se modifico"});
            }

        })
    } else {
        res.json({"Mensaje": "No concuerdan los datos"});
    }
});

router.delete('/api/alumno/:idAlumno', function(req, res){
    var idAlumno = req.params.idAlumno;
    alumno.delete(idAlumno, function(error, resultado){
        if(resultado && resultado.Mensaje === "Eliminado") {
            res.json({"Mensaje" : "/api/alumno"});
        } else {
             res.json({"Mensaje": "No se puede eliminar"});
        }

    });
});

module.exports = router;
