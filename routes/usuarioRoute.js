var express = require('express');
var Usuario = require('../model/usuario');
var router = express.Router();

router.get('/api/usuario/',function(req, res){
    Usuario.getAll(function(error,resultado){
        if(typeof resultado !== undefined){
            res.json(resultado);
        }else{
            res.json({"Mensaje":"No hay usuarios"});
        }
    });
});
router.post('/autenticar',function(req,res){
    var data = {
        nick:req.body.nick,
        contrasena:req.body.contrasena
    }
    Usuario.autenticar(data,function(err,resultado){
        if(resultado !== undefined){
            res.cookie("idUsuario",resultado[0].idUsuario);
            res.cookie("nick",resultado[0].nick);
            console.log("Se guardo la cookie");
            res.redirect("/");
        }else{
            res.json({"Mensaje":"No se ingreso el Usuario"});
        }
    });
});
module.exports = router;